set hive.exec.dynamic.partition.mode=nonstrict;
set hive.vectorized.execution.enabled=false;
set hive.merge.sparkfiles=true;
set hive.execution.engine=spark;
set spark.exectuor.num=5;
set spark.executor.cores=2;
set spark.executor.memory=4g;
set spark.default.parallelism=30;
set spark.sql.auto.repartition=true;
set spark.dynamicAllocation.maxExecutors=5;
set spark.app.name=select_hsbi;

--把列补全然
with sql1 as(
SELECT 
CASE
WHEN LENGTH(PERIOD_NUM) = 1 THEN CONCAT(PERIOD_YEAR,'0',PERIOD_NUM) ELSE CONCAT(PERIOD_YEAR,PERIOD_NUM) END MONTH_,
COMPANY_CODE, 
COMPANY_NAME,
STORE_CODE,
STORE_NAME,
CASE WHEN ACCOUNT_CODE = 'NC04010101' THEN AMOUNT ELSE 0 END INCOME_SALES,
CASE WHEN ACCOUNT_CODE = 'NC04010102' THEN AMOUNT ELSE 0 END INCOME_RENT,
CASE WHEN ACCOUNT_CODE = 'NC04010103' THEN AMOUNT ELSE 0 END INCOME_PROPERTY,
CASE WHEN ACCOUNT_CODE = 'NC04010199' THEN AMOUNT ELSE 0 END INCOME_MAINOTHER,
CASE WHEN ACCOUNT_CODE = 'NC040102'   THEN AMOUNT ELSE 0 END INCOME_OTHER,
CASE WHEN ACCOUNT_CODE = 'NC04020101' THEN AMOUNT ELSE 0 END COST_SALES,
CASE WHEN ACCOUNT_CODE = 'NC04020102' THEN AMOUNT ELSE 0 END COST_RENT,
CASE WHEN ACCOUNT_CODE = 'NC04020103' THEN AMOUNT ELSE 0 END COST_PROPERTY,
CASE WHEN ACCOUNT_CODE = 'NC04020199' THEN AMOUNT ELSE 0 END COST_MAINOTHER,
CASE WHEN ACCOUNT_CODE = 'NC040202'   THEN AMOUNT ELSE 0 END COST_OTHER,
CASE WHEN ACCOUNT_CODE = 'NC0203'     THEN AMOUNT ELSE 0 END FINANCE,
CASE WHEN ACCOUNT_CODE = 'NA010301'     THEN AMOUNT ELSE 0 END FINANCE1,
CASE WHEN ACCOUNT_CODE = 'NA010302'     THEN AMOUNT ELSE 0 END FINANCE2,
CASE WHEN ACCOUNT_CODE = 'BGAPL01'  THEN AMOUNT ELSE 0 END INCOME_TOTAL,
CASE WHEN ACCOUNT_CODE = 'BGAPL03'  THEN AMOUNT ELSE 0 END GROSS_PROFIT,
CASE WHEN ACCOUNT_CODE = 'PL04' THEN AMOUNT ELSE 0 END MASS_OF_PROFIT,
CASE WHEN ACCOUNT_CODE = 'BGAPLFY' THEN AMOUNT ELSE 0 END COST_TOTAL 
FROM BIGDATA_DWD.BGBI_RESULT_INTERFACE where store_code is not NULL and store_name is not null),



sql2 as(
----财务接口对门店数据的汇总
SELECT 
t.MONTH_,
t.COMPANY_NAME,
t.COMPANY_CODE,
t1.storename,
t1.storeid,
SUM(t.INCOME_SALES) as INCOME_SALES,
SUM(t.INCOME_RENT) as INCOME_RENT,
SUM(t.INCOME_PROPERTY) as INCOME_PROPERTY,
SUM(t.INCOME_MAINOTHER) as INCOME_MAINOTHER,
SUM(t.INCOME_OTHER) as INCOME_OTHER,
SUM(t.COST_SALES) as COST_SALES,
SUM(t.COST_RENT) as COST_RENT,
SUM(t.COST_PROPERTY) as COST_PROPERTY,
SUM(t.COST_MAINOTHER) as COST_MAINOTHER,
SUM(t.COST_OTHER) as COST_OTHER,
SUM(t.FINANCE) as FINANCE,
SUM(t.FINANCE1) as FINANCE1,
SUM(t.FINANCE2) as FINANCE2,
SUM(t.INCOME_TOTAL) as INCOME_TOTAL,
SUM(t.GROSS_PROFIT) as GROSS_PROFIT,
SUM(t.MASS_OF_PROFIT) as MASS_OF_PROFIT,
SUM(t.COST_TOTAL) as COST_TOTAL
FROM sql1 t
inner join
bigdata_dwd.hsbi_storeinf t1
on t.COMPANY_CODE = t1.erpfincode
group by 
t.month_,
t.COMPANY_NAME,
t1.storename,
t.company_code,
t1.storeid),

--门店每个月得实际营收、毛利总额、费用、利润
sql3 as(
SELECT 
MONTH_,
COMPANY_NAME,
COMPANY_CODE,
storename,
storeid,
(nvl(income_sales,0.00) + nvl(income_rent,0.00) + nvl(income_property,0.00) + nvl(income_mainother,0.00) + nvl(income_other,0.00) + nvl(INCOME_TOTAL,0.00)) as real_income,
(nvl(income_sales,0.00) + nvl(income_rent,0.00) + nvl(income_property,0.00) + nvl(income_mainother,0.00) + nvl(income_other,0.00) - nvl(cost_sales,0.00) - nvl(cost_rent,0.00) - nvl(cost_property,0.00) - nvl(cost_mainother,0.00)- nvl(cost_other,0.00) + nvl(GROSS_PROFIT,0.00)) as real_maoli,
nvl(finance,0.00) + nvl(COST_TOTAL,0.00) as real_finance,
nvl(GROSS_PROFIT,0.00) -  nvl(COST_TOTAL,0.00) + nvl(MASS_OF_PROFIT,0.00) as real_profit
FROM sql2
),

--把门店四个字段的每个月的目标加上
sql4 as(
select 
t1.MONTH_,
t1.COMPANY_NAME,
t1.COMPANY_CODE,
t1.storename,
t1.storeid,
t2.operatingrevenue as budget_real_income,
t1.real_income,
t2.grossprofit as budget_maoli,
t1.real_maoli,
t2.costbudget as budget_finance,
t1.real_finance,
t2.grossprofit-t2.costbudget as budget_profit,
t1.real_profit
FROM sql3 t1
left join bigdata_dwd.hsbi_annualbudget t2
on t1.storeid = t2.storeid  AND  (CASE WHEN LENGTH(t2.month_) = 1 THEN CONCAT(t2.year_ ,'0',t2.month_) ELSE CONCAT(t2.year_,t2.month_) END ) = t1.month_
),
--环比和同比
sql5 as(
SELECT 
t1.MONTH_,
t1.COMPANY_NAME,
t1.COMPANY_CODE,
t1.storename,
t1.storeid,
t1.budget_real_income,
t1.real_income,
t2.real_income as hb_real_income,
t3.real_income as tb_real_income,
t1.budget_maoli,
t1.real_maoli,
t2.real_maoli as hb_real_maoli,
t3.real_maoli as tb_real_maoli,
t1.budget_finance,
t1.real_finance,
t2.real_finance as hb_real_finance,
t3.real_finance as tb_real_finance,
t1.budget_profit,
t1.real_profit,
t2.real_profit as hb_real_profit,
t3.real_profit as tb_real_profit
FROM sql4 t1
left join sql4 t2
on concat(substr(t1.MONTH_,1,4),'-',substr(t1.MONTH_,5,2),'-01') = add_months((concat(substr(t2.MONTH_,1,4),'-',substr(t2.MONTH_,5,2),'-01')),1) AND 	t1.storeid = t2.storeid
left join sql4 t3
on  concat(substr(t1.MONTH_,1,4),'-',substr(t1.MONTH_,5,2),'-01')  = add_months((concat(substr(t3.MONTH_,1,4),'-',substr(t3.MONTH_,5,2),'-01')),12) AND  t1.storeid = t3.storeid
),
--累计的目标值和实际值
sql6 as(
SELECT
MONTH_,
COMPANY_NAME,
COMPANY_CODE,
storename,
storeid,
budget_real_income,
real_income,
hb_real_income,
tb_real_income,
SUM(budget_real_income) over(partition by SUBSTR(month_,1,4) ,storeid  order by  month_ asc rows between unbounded preceding and current row ) as budget_real_income_ytd,
SUM(real_income) over(partition by SUBSTR(month_,1,4) ,storeid  order by month_ asc rows between unbounded preceding and current row) as real_income_ytd,
budget_maoli,
real_maoli,
hb_real_maoli,
tb_real_maoli,
SUM(budget_maoli) over(partition by SUBSTR(month_,1,4) ,storeid  order by month_ asc rows between unbounded preceding and current row) as budget_maoli_ytd,
SUM(real_maoli) over(partition by SUBSTR(month_,1,4) ,storeid  order by month_ asc rows between unbounded preceding and current row) as real_maoli_ytd,
budget_finance,
real_finance,
hb_real_finance,
tb_real_finance,
SUM(budget_finance) over(partition by SUBSTR(month_,1,4) ,storeid  order by  month_ asc rows between unbounded preceding and current row )as budget_finance_ytd,
SUM(real_finance) over(partition by SUBSTR(month_,1,4) ,storeid  order by  month_ asc rows between unbounded preceding and current row) as real_finance_ytd,
budget_profit,
real_profit,
hb_real_profit,
tb_real_profit,
SUM(budget_profit) over(partition by SUBSTR(month_,1,4) ,storeid  order by  month_ asc rows between unbounded preceding and current row) as budget_profit_ytd,
SUM(real_profit) over(partition by SUBSTR(month_,1,4) ,storeid  order by  month_ asc rows between unbounded preceding and current row) as real_profit_ytd
FROM sql5
),
--累计数算同比
sql7 as(
select 
t1.MONTH_,
t1.COMPANY_NAME,
t1.COMPANY_CODE,
t1.storename,
t1.storeid,
t1.budget_real_income,
t1.real_income,
t1.hb_real_income,
t1.tb_real_income,
t1.budget_real_income_ytd,
t2.budget_real_income_ytd as budget_real_income_ytd_lastyear,
t1.real_income_ytd,
t2.real_income_ytd as real_income_ytd_lastyear,
t1.budget_maoli,
t1.real_maoli,
t1.hb_real_maoli,
t1.tb_real_maoli,
t1.budget_maoli_ytd,
t2.budget_maoli_ytd as budget_maoli_ytd_lastyear,
t1.real_maoli_ytd,
t2.real_maoli_ytd as real_maoli_ytd_lastyear,
t1.budget_finance,
t1.real_finance,
t1.hb_real_finance,
t1.tb_real_finance,
t1.budget_finance_ytd,
t2.budget_finance_ytd as budget_finance_ytd_lastyear,
t1.real_finance_ytd,
t2.real_finance_ytd as real_finance_ytd_lastyear,
t1.budget_profit,
t1.real_profit,
t1.hb_real_profit,
t1.tb_real_profit,
t1.budget_profit_ytd,
t2.budget_profit_ytd as budget_profit_ytd_lastyear,
t1.real_profit_ytd,
t2.real_profit_ytd as real_profit_ytd_lastyear
FROM sql6  t1
left join sql6 t2
on concat(substr(t1.MONTH_,1,4),'-',substr(t1.MONTH_,5,2),'-01') = add_months((concat(substr(t2.MONTH_,1,4),'-',substr(t2.MONTH_,5,2),'-01')),12) AND 	t1.storeid = t2.storeid
)

SELECT * FROM sql7;



