with sql0 as(
select
`date` as beginMonth,
regexp_replace(substr(`date`,1,7),'-','') as month_
from bigdata_dwd.time_dimension
where year>=floor(20180101/10000) and day = 1 and `date`<=trunc(current_date(),'MM')
),

sql2 as(
select
compactNo,
storeid,
departmentid,
supplierCode,
signbrandcode,
substr(beginDate,1,10) as beginDate,
substr(endDate,1,10) as endDate,
--修改经营天数
--case when substr(endDate,1,10)>=add_months(beginMonth,1) then datediff(add_months(beginMonth,1),beginMonth) else datediff(enddate,beginMonth)+1 end as daycount,
case
when beginMonth<=beginDate and add_months(beginMonth,1)<=endDate then datediff(add_months(beginMonth,1),beginDate)
when beginMonth<=beginDate and add_months(beginMonth,1)>=endDate then datediff(endDate,beginDate)+1
when beginMonth>=beginDate and add_months(beginMonth,1)<=endDate then datediff(add_months(beginMonth,1),beginMonth)
else datediff(endDate,beginMonth)+1
end as daycount,
--datediff(add_months(beginMonth,1),beginMonth) as daycount,
regexp_replace(substr(beginMonth,1,7),'-','') as month_
from
bigdata_dwd.hsbi_compact_basicinf,sql0
where
--beginMonth>=substr(beginDate,1,10) and beginMonth<=substr(endDate,1,10)
substr(beginMonth,1,7)>=substr(beginDate,1,7) and substr(beginMonth,1,7)<=substr(endDate,1,7)
)

SELECT * from sql0;