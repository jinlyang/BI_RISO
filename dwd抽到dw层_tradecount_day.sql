set hive.execution.engine=mr;
set hive.vectorized.execution.enabled=false;
set hive.exec.dynamic.partition.mode=nonstrict;

INSERT overwrite table riso_dw.riso_tradecount_day partition (dt)
SELECT
DATE_FORMAT(t1.orderdate,'yyyy') as orderyear,
DATE_FORMAT(t1.orderdate,'yyyyMM') as ordermonth,
DATE_FORMAT(t1.orderdate,'yyyyMMdd') as orderdate, 
t1.storeid,
COUNT(t1.orderid) as tradecount,
from_unixtime(unix_timestamp(),'yyyy-MM-dd HH:mm:ss') as insertdate, 
DATE_FORMAT(t1.orderdate,'yyyyMM') as dt
FROM riso_dwd.riso_order t1
group by t1.storeid ,t1.orderdate 