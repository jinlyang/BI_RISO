set hive.exec.dynamic.partition.mode=nonstrict;
set hive.vectorized.execution.enabled=false;
set hive.merge.sparkfiles=true;
set hive.execution.engine=spark;
set spark.exectuor.num=5;
set spark.executor.cores=2;
set spark.executor.memory=4g;
set spark.default.parallelism=30;
set spark.sql.auto.repartition=true;
set spark.dynamicAllocation.maxExecutors=5;
set spark.app.name=select_hsbi;

with sql1 as(
SELECT
t1.`date` as orderday,
t2.bucode,
t2.buname,
t2.storeid,
t2.storename
from bigdata_dwd.time_dimension t1 
full join bigdata_dwd.hsbi_storeinf t2 
on 1=1),
--������ ���Ƶ�
sql2 as(
SELECT 
SUBSTR(t1.orderdate,1,10) as orderdate,
t1.storeid,
SUM(t1.bargainamount) as nono
FROM bigdata_dwd.hsbi_orderinf t1 
where
t1.balancetype in ('1','2','3','5')  and t1.orderchannel in ('1','3','4')
group by 
t1.storeid,
SUBSTR(t1.orderdate,1,10) 
),
--������ ���Ƶ�
sql3 as(
SELECT 
SUBSTR(t1.orderdate,1,10) as orderdate,
t1.storeid,
SUM(t1.bargainamount) as noYundian
FROM bigdata_dwd.hsbi_orderinf t1 
where
t1.balancetype in ('1','2','3','5')  and t1.orderchannel in ('1','2','3','4')
group by 
t1.storeid,
SUBSTR(t1.orderdate,1,10) 
),
--������ ���Ƶ�
sql4 as(
SELECT 
SUBSTR(t1.orderdate,1,10) as orderdate,
t1.storeid,
SUM(t1.bargainamount) as zulinno
FROM bigdata_dwd.hsbi_orderinf t1 
where
t1.balancetype in ('1','2','3','5','4')  and t1.orderchannel in ('1','3','4')
group by 
t1.storeid,
SUBSTR(t1.orderdate,1,10) 
),
--������ ���Ƶ�
sql5 as(
SELECT 
SUBSTR(t1.orderdate,1,10) as orderdate,
t1.storeid,
SUM(t1.bargainamount) as zulinyundian
FROM bigdata_dwd.hsbi_orderinf t1 
where
t1.balancetype in ('1','2','3','5','4')  and t1.orderchannel in ('1','2','3','4')
group by 
t1.storeid,
SUBSTR(t1.orderdate,1,10) 
)

SELECT 
t1.orderday,
t1.bucode,
t1.buname,
t1.storeid,
t1.storename,
t2.nono,
t3.noYundian,
t4.zulinno,
t5.zulinyundian
FROM sql1 t1
left join sql2 t2 on t2.orderdate=t1.orderday AND t2.storeid = t1.storeid
left join sql3 t3 on t3.orderdate=t1.orderday AND t3.storeid = t1.storeid
left join sql4 t4 on t4.orderdate=t1.orderday AND t4.storeid = t1.storeid
left join sql5 t5 on t5.orderdate=t1.orderday AND t5.storeid = t1.storeid;
