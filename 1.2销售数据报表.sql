set hive.exec.dynamic.partition.mode=nonstrict;
set hive.vectorized.execution.enabled=false;
set hive.merge.sparkfiles=true;
set hive.execution.engine=spark;
set spark.exectuor.num=5;
set spark.executor.cores=2;
set spark.executor.memory=4g;
set spark.default.parallelism=30;
set spark.sql.auto.repartition=true;
set spark.dynamicAllocation.maxExecutors=5;
set spark.app.name=select_hsbi;

--̫���˲�д��

SELECT 
SUM(t1.innum)
from bigdata_dwd.hsbi_passengerflow t1
WHERE t1.storeid ='001320' and SUBSTR(t1.flowtime,1,10)='2021-10-01';