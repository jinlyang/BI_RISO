set hive.exec.dynamic.partition.mode=nonstrict;
set hive.vectorized.execution.enabled=false;
set hive.merge.sparkfiles=true;
set hive.execution.engine=spark;
set spark.exectuor.num=5;
set spark.executor.cores=4;
set spark.executor.memory=5g;
set spark.default.parallelism=30;
set spark.sql.auto.repartition=true;
set spark.dynamicAllocation.maxExecutors=5;
set spark.app.name=select_hsbi;

--门店 柜组关联地块号，楼层，面积
with sql1 as(
select
t1.storeid,
t1.departmentid,
t2.blockno,
t2.floor_,
t2.businessarea
from
bigdata_dw.hsbi_department t1
left join
bigdata_dwd.hsbi_blockinf t2
on t1.storeid=t2.storeid and t1.blockno = t2.blockid),
--关联合同
sql2 as(
SELECT 
t1.*,
t2.compactno,
t2.suppliercode,
t2.signbrandcode,
SUBSTR(t2.begindate,1,10) as compact_begindate,
SUBSTR(t2.enddate,1,10) as compact_enddate
FROM sql1 t1
left join bigdata_dwd.hsbi_compact_basicinf t2
on t1.storeid =t2.storeid and t1.departmentid =t2.departmentid
),
--根据合同匹配供应商品类、原品牌、供应商编码合同号
sql3 as(
SELECT 
t1.storeid,
t1.departmentid ,
t1.blockno ,
t1.floor_,
t1.businessarea,
t1.compactno,
t1.compact_begindate,
t1.compact_enddate,
t2.supplierMercClassName,
t3.brandname as signBrandName,
t1.suppliercode
FROM sql2 t1 
left join bigdata_dwd.hsbi_supplierinf t2
on t1.suppliercode = t2.supplierCode
left join
bigdata_dwd.hsbi_mainmercbrand t3
on t1.signBrandCode = t3.brandcode
)

SELECT * FROM sql3;