set hive.execution.engine=mr;
set hive.vectorized.execution.enabled=false;
set hive.exec.dynamic.partition.mode=nonstrict;
--set spark.sql.broadcastTimeout=1800;
--set spark.app.name=riso_dwd_to_dw;


with sql1 as(
--月销售额
SELECT
t1.storeid,
t1.ordermonth ,
SUM(t1.tradeamount) as tradeamount 
FROM riso_dw.riso_order_day t1
GROUP BY 
t1.storeid,
t1.ordermonth
),
--月成本
sql2 as(
SELECT
t2.storeid,
t2.recordmonth ,
SUM(t2.tradecost) as tradecost 
FROM riso_dw.riso_cost_day t2
GROUP BY 
t2.storeid,
t2.recordmonth
),
--月目标
sql3 as(
SELECT
t3.storeid ,
t3.year +t3.month as budgetmonth,
t3.salesbudget ,
t3.profitbudget 
FROM riso_dwd.riso_budget t3
GROUP BY 
t3.storeid ,
t3.year +t3.month as budgetmonth
)

