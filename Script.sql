set hive.exec.dynamic.partition.mode=nonstrict;
set hive.vectorized.execution.enabled=false;
set hive.merge.sparkfiles=true;
set hive.execution.engine=spark;
set spark.exectuor.num=5;
set spark.executor.cores=2;
set spark.executor.memory=4g;
set spark.default.parallelism=30;
set spark.sql.auto.repartition=true;
set spark.dynamicAllocation.maxExecutors=5;
set spark.app.name=bigdata_dm.dm_business_rent_detail_group_brand;



--补全维度
with sql1 as (
SELECT
	t2.area ,
	t2.city_name ,
	t1.*,
	t3.product_name
FROM user_visit_action t1
left join city_info t2 
on t2.city_id = t1.city_id
left join product_info t3
on t1.click_product_id = t3.product_id
WHERE t1.click_product_id >-1
),
--计算每个区域点击总次数
sql2 as(
SELECT 
t1.area,
t1.product_name,
COUNT(t1.click_product_id) as clickNum 
FROM sql1 t1 
group by 
t1.area,
t1.product_name
),
sql3 as(
SELECT 
t.*,
RANK() over(PARTITION by t.area order by t.clicknum DESC) as rank
FROM sql2 t
),
--每个区域点击量前三名的商品
sql4 as(
select 
*
FROM sql3 t1
WHERE t1.RANK <=3
),
sql5 as(
SELECT 
t1.area,
t1.city_name,
t1.product_name,
COUNT(t1.click_product_id) as clickNum 
FROM sql1 t1
group by
t1.area,
t1.city_name,
t1.product_name
),
sql6 as(
SELECT 
t1.area,
t1.product_name,
t1.clicknum as total_click,
t2.city_name,
t2.clicknum as city_click,
ROUND(t2.clicknum/t1.clicknum*100,2)  as percentNum
FROM sql4 t1
left join sql5 t2
on t1.area = t2.area and t1.product_name = t2.product_name
),
sql7 as(
SELECT 
t.*,
RANK () over(PARTITION by t.area,t.product_name order by t.percentNum DESC) as rank
FROM sql6 t
),
sql8 as(
SELECT * FROM sql7 WHERE rank<=2
)

SELECT 
t.area,
t.product_name,
t.total_click,
concat_ws(',',collect_set(t.city_name)) as country,
concat_ws(',',collect_set((cast(t.percentnum as String)))) as country
FROM sql8 t
group by 
t.area,
t.product_name,
t.total_click;


